package com.wiyotravel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
//import com.wiyotravel.R;

public class WiyoTravelActivity extends AppCompatActivity {

    private WebView turtleWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wiyo_travel);
        turtleWebView=(WebView)findViewById(R.id.webview);
        WebSettings webSettings = turtleWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        turtleWebView.loadUrl("http://www.wiyotravel.com/");
        //http://www.wiyotravel.com/
        turtleWebView.setWebViewClient(new WebViewClient()); //prevent to open your url in browser
    }

    @Override
    public void onBackPressed() {
        if(turtleWebView.canGoBack()){
            turtleWebView.goBack();
        }else{
            super.onBackPressed(); //Close app on final back button press
        }
    }
}

